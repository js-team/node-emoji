# Installation
> `npm install --save @types/node-emoji`

# Summary
This package contains type definitions for node-emoji (https://github.com/omnidan/node-emoji#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/node-emoji.

### Additional Details
 * Last updated: Tue, 13 Sep 2022 15:02:47 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Tristan Jones](https://github.com/jonestristand), [styu](https://github.com/styu), and [rimiti](https://github.com/rimiti).
